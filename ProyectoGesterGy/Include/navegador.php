
<!-- 
    Para no tener que repetir <nav> en todas las páginas. se utilizará más adelante en todas las html (cuando se pasen a php), de 
    momento no las paso a php porque son de otra asignatura y de momento deben quedarse en html. esta es de ejemplo para el ejercicio 
    de Entornos de desarrollo
 -->

<nav class="tablaNav">

<ul>
    <li><a <?php echo ($page == 'Facturas.html') ? "class='active'" : ""; ?> href="Facturas.html">Facturas</a></li>
    <li><a <?php echo ($page == 'formulario.html') ? "class='active'" : ""; ?>  href="formulario.php">Formulario</a></li>
    <li><a <?php echo ($page == 'ComparadorOfertas.html') ? "class='active'" : ""; ?> href="ComparadorOfertas.html">Comparador</a></li>
    <li><a <?php echo ($page == 'Registrarme.html') ? "class='active'" : ""; ?> href="Registrarme.html">Registrarme</a></li>
    <li><a <?php echo ($page == 'video.html') ? "class='active'" : ""; ?> href="video.html">Video</a></li>
    <li><a <?php echo ($page == 'Contactos.htm') ? "class='active'" : ""; ?> href="Contactos.html">Contacto</a></li>
</ul>

</nav>