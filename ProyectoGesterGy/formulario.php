<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link rel="stylesheet" href="CSS/estilo.css">
</head>

<body>
    <div id="fondo">
        <?php
            include 'Include/Cabecera.php';
            cabecera();

            $page = 'formulario.html';
            include ('Include/navegador.php');
        ?>

        <section>


            <article>

                <h1 id="etiquetaFormulario">Formulario de Consulta</h1>
                <br>


                <form action="PHP/respuestaFormulario.php" method="post" autocomplete="on">
                
                <label for="nombre">Nombre:</label>
                <input type="text" id="nombre" name="nombre" width="50" maxlength="30" required>
                <label for="apellidos">Apellidos:</label>
                <input type="text" id="apellidos" name="apellidos" width="50" maxlength="30">
                <br><br>
    
                <label for="metrosCuadrados">Tamaño de tu vivienda (m<sup>2</sup>):</label>
                <input type="number" id="numero" name="metrosCuadrados">
                <br><br>

                <label for="numeroDeHabitaciones">Número de Habitaciones:</label>
                <input type="number"  width="50" name="numeroDeHabitaciones" max="8" min="1">
                <br><br>

                <label for="tipoVivienda">Tipo de Vivienda:</label>
                <input type="radio" name="tamanyoVivienda" Value="Pequeña" checked>Pequeña [40 - 60 m<sup>2</sup>]
                <input type="radio" name="tamanyoVivienda" Value="Mediana">Mediana [60 - 100 m<sup>2</sup>]
                <input type="radio" name="tamanyoVivienda" Value="Grande">Grande [100 - 250 m<sup>2</sup>]
                <br><br>

                <label for="calidadAislamientos">Calidad del aislamiento de la vivienda:</label>
                <input type="number" id="puntuacion" name="calidadAislamientos" max="5" min="1" placeholder="1 bajo a 5 excelente" width="50" required>
                <br><br>

                <label for="tipoClima">Tipo de Clima:</label>
                <select name="ciudad" id="ciudad">
                    <option value="1" spellcheck="Elegir ciudad">Frio</option>
                    <option value="2">Templado</option>
                    <option value="3" selected>Caluroso</option>
                </select>
                <br><br><br>

                <label for="dudas">Comentario:</label><br>
                <textarea name="dudas" id="dudas" cols="75" rows="4" placeholder="Ejemplo: No tengo tejado para instalar placas fotovoltaicas."></textarea>
                <br><br>

                <label for="checkbox">¿Que tecnología prefieres?:</label><br><br>
                <input type="checkbox" name="Tecnología1" value="1" >Eólica
                <br>
                <input type="checkbox" name="Tecnología2" value="2" >Fotovoltaica
                <br>
                <input type="checkbox" name="Tecnología3" value="3" >Geotérmica
                <br>
                <input type="checkbox" name="Tecnología4" value="4" >Pélet
                <br><br>


                <button id="boton" type="submit" name="submit" value="0">Enviar</button>
                <br><br>

                </form>

            </article>
        </section>



        
        <?php
        include "Include/pie.php";
        pie();
        ?>


    </div>
</body></html>
