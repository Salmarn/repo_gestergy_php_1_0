<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link rel="stylesheet" type="text/css" href="../CSS/estilo.css">
</head>

<body <?php print 'class='.$_REQUEST["tamanyoVivienda"].'' ?> >
    <div id="fondo">
        <?php
            include '../Include/Cabecera.php';
            cabecera();
        ?>

        <nav class="tablaNav">

        <ul>
            <li><a href="../Facturas.html">Facturas</a></li>
            <li><a class = "active" href="../formulario.php">Formulario</a></li>
            <li><a href="../ComparadorOfertas.html">Comparador</a></li>
            <li><a href="../Registrarme.html">Registrarme</a></li>
            <li><a href="../video.html">Video</a></li>
            <li><a href="../Contactos.html">Contacto</a></li>
        </ul>

        </nav>
        <section>


            <article class="formulario">

                <h1 id="etiquetaFormulario">Propuesta Tras su Consulta</h1>
                <br>


                <div class="formulario0">
                    <?php                    
                    if ($_REQUEST["submit"]==0) {
                        print "<p> Hola " .$_REQUEST["nombre"]." " .$_REQUEST["apellidos"]. "  bienvenido! </p>";
                    }                                        
                    ?>

                    <P>Sus datos son:</P>
                </div>
                
                
                <div class="formulario1">
                    
                    <p>Tamaño de casa: <?php print $_REQUEST["tamanyoVivienda"] ?></p>
                    
                </div>

                <div class="grupo1">
                    
                    <div class="formulario2">
                        <p>Numero de Habitaciones:</p> 
                        <?php 
                        $numeroHab = $_REQUEST["numeroDeHabitaciones"];
                        for ($i=0; $i < $numeroHab; $i++) { 
                            print  '<img src="../images/cuna.png" alt="imagenHabitaciones">';
                        } 
                        ?>          
                    </div>

                
                
                    <div class="formulario3">
                        <p>Calidad del aislamiento:</p>
                        <?php print '<img src="../images/Star_rating_'.$_REQUEST["calidadAislamientos"].'_of_5.png" alt="Rating">' ?>
                    
                    
                    </div>
                </div>
                
                    <div class="formulario4">
                        <p>Sugerencia:</p>
                        <?php 
                            if ($_REQUEST["ciudad"]==1) {
                                if ($_REQUEST["calidadAislamientos"]<3) {
                                    print '<p>El clima de su ciudad es frio y el aislante de su casa es insuficiente, 
                                    por lo tanto se recomienda mejorar el aislamiento térmico y posteriorment utilizar un sistema de pellets. '; 
                                }
                                if ($_REQUEST["calidadAislamientos"]==3) {
                                    print '<p>El clima de su ciudad es frio y el aislante de su casa es medio, 
                                    por lo tanto se recomienda instalar un sistema de pellets.';
                                }
                                if ($_REQUEST["calidadAislamientos"]>3) {
                                    print '<p>El clima de su ciudad es frio y el aislante de su casa adecuado,
                                    por lo tanto se recomienda instalar un sistema de generación fotovoltaica o geotérmia. '; 
                                }
                               
                            }
                            if ($_REQUEST["ciudad"]==2) {
                                if ($_REQUEST["calidadAislamientos"]<3) {
                                    print '<p>El clima de su ciudad es templado y el aislante de su casa es insuficiente, 
                                    por lo tanto se recomienda mejorar el aislamiento térmico o instalar un sistema de generación fotovoltaica. '; 
                                }
                                if ($_REQUEST["calidadAislamientos"]==3) {
                                    print '<p>El clima de su ciudad es templado y el aislante de su casa es medio, 
                                    por lo tanto se recomienda instalar un sistema de pellets.';
                                }
                                if ($_REQUEST["calidadAislamientos"]>3) {
                                    print '<p>El clima de su ciudad es templado y el aislante de su casa adecuado,
                                    por lo tanto podría considerar adaptar su casa a passivhauss. '; 
                                }
                               
                            }
                            if ($_REQUEST["ciudad"]==3) {
                                if ($_REQUEST["calidadAislamientos"]<3) {
                                    print '<p>El clima de su ciudad es cálido y el aislante de su casa es insuficiente, 
                                    por lo tanto se recomienda mejorar el aislamiento térmico y posteriorment utilizar un sistema de geotérmia. '; 
                                }
                                if ($_REQUEST["calidadAislamientos"]==3) {
                                    print '<p>El clima de su ciudad es cálido y el aislante de su casa es medio, 
                                    por lo tanto se recomienda instalar un sistema de generación fotovoltaico.';
                                }
                                if ($_REQUEST["calidadAislamientos"]>3) {
                                    print '<p>El clima de su ciudad es calido y el aislante de su casa adecuado,
                                    por lo tanto se recomienda instalar un sistema de protección solar. '; 
                                }
                               
                            }
                        
                        ?>
                    </div>
                    
                    <!-- <div class="formulario4">
                        <p>La tecnología preferida es:</p>
                        <?php 
                        if ($_REQUEST["Tecnología1"] == "") {
                           $_REQUEST["Tecnología1"] = "Sintecnologia";
                        } else  print '<img src="../images/img_3800.jpg" width=125 height=75 alt="aero">';
                       
                        if ($_REQUEST["Tecnología2"] == "") {
                           $_REQUEST["Tecnología2"] = "Sintecnologia";
                        } else  {print '<img src="../images/img_3800.jpg" width=125 height=75 alt="aero">';}
                       
                        if ($_REQUEST["Tecnología3"] == "") {
                           $_REQUEST["Tecnología3"] = "Sintecnologia";
                        } else { print '<img src="../images/img_3800.jpg" width=125 height=75 alt="aero">';}
                        
                        if ($_REQUEST["Tecnología4"] == "") {
                           $_REQUEST["Tecnología4"] = "Sintecnologia";
                        } else { print '<img src="../images/img_3800.jpg" width=125 height=75 alt="aero">';}
                        ?> -->
                    
                    </div>
                    <!-- No consigo que cuando una de las tecnologías no está elegida no aparezca, sale error -->
                    <!-- <?php 
                    print "<pre>";
                    print_r($_REQUEST);
                    print "</pre>\n";
                    ?> -->
                </div>

            </article>
        </section>

        
        <?php
            include "../Include/pie.php";
            pie();
        ?>

    </div>
</body></html>
