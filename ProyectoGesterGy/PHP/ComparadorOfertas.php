<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Comparador de Ofertas</title>
    <link rel="stylesheet" href="../CSS/estilo.css">
</head>

<body>
    <div id="fondo">
        <header>
            <h1>GesterGy</h1>

        </header>

        <nav class="tablaNav">
            

            <ul>
                <li><a href="../Facturas.html">Facturas</a></li>
                <li><a href="../formulario.php">Formulario</a></li>
                <li><a class="active" href="../ComparadorOfertas.html">Comparador</a></li>
                <li><a href="../Registrarme.html">Registrarme</a></li>
                <li><a href="../video.html">Video</a></li>
                <li><a href="../Contactos.html">Contacto</a></li>
            </ul>

        </nav>

        <?php
            
           /*  print "<pre>";
            print_r($_REQUEST);
            print "</pre>\n";
                      */

            if ($_REQUEST["Consumo_kWh"]=="") {
                $_REQUEST["Consumo_kWh"]=0;
            }
            if ( $_REQUEST["PotenciaContratada_kW"]=="") {
                $_REQUEST["PotenciaContratada_kW"]=0;
            }
           
           

            $consumoEnergiaAnual = $_REQUEST["Consumo_kWh"];
            $potenciaContratada = $_REQUEST["PotenciaContratada_kW"];
            
            $endesa_Eur_kWh = 0.10001;
            $endesa_Eur_kW_dia = 0.10001;

            $iberdrola_Eur_kWh = 0.20001;
            $iberdrola_Eur_kW_dia = 0.20001;

            $fenie_Eur_kWh = 0.09443;
            $fenie_Eur_kW_dia = 0.10394;

            $naturgy_Eur_kWh = 0.40001;
            $naturgy_Eur_kW_dia = 0.40001;

            $holaLuz_Eur_kWh = 0.50001;
            $holaLuz_Eur_kW_dia = 0.50001;

            function calculaCosteAnual ($arg1, $arg2, $arg3, $arg4) {
                
                $costeAnual = ($arg1 * $arg3) + ($arg2 * $arg4 * 365);
                return $costeAnual;

            }


        ?>


        <section>
            <article>
                <h1 id="etiquetaFormulario">Comparador de Ofertas</h1>

                <br>
                <div class="comparador">
                    <br>

                    <form action="ComparadorOfertas.php" method="post">
                        
                        <label for="Consumo_kWh">Introduce kWh consumidos anualmente:</label>
                        <input type="number" id="kWh" name="Consumo_kWh">
                        <br><br>
                        
                        <label for="PotenciaContratada_kW">Introduce tu potencia contratada en kW:</label>
                        <input type="number" id="kW" name="PotenciaContratada_kW">
                        <br><br>

                        <button type="submit" id="boton" value="submit">Enviar</button>
                        <br><br><br>

                    </form> 

                    <h2 ALIGN="center">Cálculo de Ofertas:</h2>
                    <br>
                    <br>

                    <table class="tablaComparativa">
                        <tr>
                            <th Align="left">Compañia</th>
                            <th>Coste energía (€/kWh)</th>
                            <th>Coste potencia (€/kW día)</th>
                            <th>Coste Anual (€/año)</th>
                        </tr>
                        <tr>
                            <td Align="left">Endesa</td>
                            <td><?php print $endesa_Eur_kWh   ?></td>
                            <td><?php print $endesa_Eur_kW_dia   ?></td>
                            <td><?php print $costeAnual = calculaCosteAnual ($endesa_Eur_kWh, $endesa_Eur_kW_dia, $consumoEnergiaAnual, $potenciaContratada)   ?></td>
                        </tr>
                        <tr>
                            <td Align="left">Iberdrola</td>
                            <td><?php print $iberdrola_Eur_kWh   ?></td>
                            <td><?php print $iberdrola_Eur_kW_dia   ?></td>
                            <td><?php print $costeAnual = calculaCosteAnual ($iberdrola_Eur_kWh, $iberdrola_Eur_kW_dia, $consumoEnergiaAnual, $potenciaContratada)   ?></td>
                        </tr>
                        <tr>
                            <td Align="left">Fenie Energía</td>
                            <td><?php print $fenie_Eur_kWh   ?></td>
                            <td><?php print $fenie_Eur_kW_dia   ?></td>
                            <td><?php print $costeAnual = calculaCosteAnual ($fenie_Eur_kWh, $fenie_Eur_kW_dia, $consumoEnergiaAnual, $potenciaContratada)   ?></td>
                        </tr>
                        <tr>
                            <td Align="left">Naturgy</td>
                            <td><?php print $naturgy_Eur_kWh   ?></td>
                            <td><?php print $naturgy_Eur_kW_dia   ?></td>
                            <td><?php print $costeAnual = calculaCosteAnual ($naturgy_Eur_kWh, $naturgy_Eur_kW_dia, $consumoEnergiaAnual, $potenciaContratada)   ?></td>
                        </tr>
                        <tr>
                            <td Align="left">Hola Luz</td>
                            <td><?php print $holaLuz_Eur_kWh   ?></td>
                            <td><?php print $holaLuz_Eur_kW_dia   ?></td>
                            <td><?php print $costeAnual = calculaCosteAnual ($holaLuz_Eur_kWh, $holaLuz_Eur_kW_dia, $consumoEnergiaAnual, $potenciaContratada)   ?></td>
                        </tr>
                    </table>
                
                </div>


            </article>
        </section>

        <footer>
            <br>
            <p>Volver a la <a href="PaginaPrincipal.html">Página Principal</a></p>
            <p>Volver a la <a href="index.html">Página de Inicio</a></p>
        </footer>
    </div>
</body></html>
